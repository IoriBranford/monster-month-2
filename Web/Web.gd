tool
extends Node2D

# class member variables go here, for example:
export var num_slices = 20
export var num_rings = 10
export var ring_thickness = 30
export var color = Color(1,1,1)
export var line_width = 1
export var line_antialias = false
export var center_radius = 60

var current_cell = Vector2()

func _ready():
	var slicegap = deg2rad(2)
	var angle = 0
	var slicearc = 2*PI/num_slices
	var navpoly = $PlayerNavigation/Poly.navpoly
	if navpoly.get_outline_count() > 0:
		return
	var ringgap = 3
	var outerpoints = PoolVector2Array()
	for s in range(0, num_slices):
		var cell = Vector2(num_rings-1, s)
		outerpoints.append(get_cell_center(cell, ringgap))
	navpoly.add_outline(outerpoints)
	for s in range(0, num_slices):
		for r in range(0, num_rings-1):
			var cell = Vector2(r, s)
			var points = PoolVector2Array()
			points.append(get_cell_center(cell, ringgap, -slicegap))
			cell.y -= 1
			points.append(get_cell_center(cell, ringgap, slicegap))
			cell.x += 1
			points.append(get_cell_center(cell, -ringgap, slicegap))
			cell.y += 1
			points.append(get_cell_center(cell, -ringgap, -slicegap))
			navpoly.add_outline(points)
	navpoly.make_polygons_from_outlines()

func _process(delta):
	var mousepos = get_viewport().get_mouse_position()
	var mousecell = point_to_cell(mousepos)
	select_cell(mousecell)
	var arachne = $PlayerCurrentPath/Arachne
	var dest = mousepos - position
	if mousecell.x >= 0:
		dest = get_cell_center(mousecell)
	$PlayerFoundPath.points = $PlayerNavigation.get_simple_path(arachne.position, dest)
	
func select_cell(cell):
	if cell == current_cell:
		return
	current_cell = cell
	var ring = cell.x
	if ring < 0 || ring >= num_rings:
		$CellSelection.hide()
		return
	$CellSelection.show()
	var polygon = PoolVector2Array()
	var slice = cell.y
	var slicearc = 2*PI/num_slices
	var angle = (.5 + slice) * slicearc
	var prevangle = angle - slicearc
	var dist = center_radius + ring*ring_thickness
	var nextdist = dist + ring_thickness
	var unitv = Vector2(cos(angle), sin(angle))
	var prevunitv = Vector2(cos(prevangle), sin(prevangle))
	polygon.append(prevunitv*dist)
	polygon.append(unitv*dist)
	polygon.append(unitv*nextdist)
	polygon.append(prevunitv*nextdist)
	$CellSelection.polygon = polygon

func point_to_cell(point):
	assert(typeof(point)==TYPE_VECTOR2)
	var topoint = point - position
	var dist = topoint.length() - center_radius
	var ring = floor(dist / (ring_thickness))
	var slicearc = 2*PI/num_slices
	var angle = atan2(topoint.y, topoint.x)
	var slice = floor((angle + slicearc/2)/slicearc)
	return Vector2(ring, slice)

func get_cell_center(cell, offsetdist=0, offsetangle=0):
	if cell.x < 0:
		return Vector2()
	var slicearc = 2*PI/num_slices
	var a = slicearc*cell.y + offsetangle
	var r = center_radius + (cell.x + .5)*ring_thickness*cos(slicearc/2) + offsetdist
	return Vector2(r*cos(a), r*sin(a))

func _draw():
	var slicearc = 2*PI/num_slices
	
	var angle = slicearc/2
	var prevangle = -angle
	var prevradiusvec = Vector2(cos(prevangle), sin(prevangle))
	var radius = get_viewport().size.length()/2
	for s in range(0, num_slices):
		var radiusvec = Vector2(cos(angle), sin(angle))
		draw_line(Vector2(), radius*radiusvec, color, line_width, line_antialias)
		var ringdist = center_radius
		for r in range(0, num_rings+1):
			draw_line(ringdist*radiusvec, ringdist*prevradiusvec, color, line_width, line_antialias)
			ringdist += ring_thickness
		prevradiusvec = radiusvec
		angle += slicearc

func _input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == 1:
		var arachne = $PlayerCurrentPath/Arachne
		var pathpoints = $PlayerFoundPath.points
		var curve = $PlayerCurrentPath.curve
		curve.clear_points()
		for p in pathpoints:
			curve.add_point(p)
		arachne.start_walk()
