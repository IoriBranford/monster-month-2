extends PathFollow2D

export var speed = 360
var current_speed = 0

func _process(delta):
	var direction = Vector2()
	if current_speed > 0:
		var lastpos = position
		offset += current_speed*delta
		if unit_offset >= 1:
			current_speed = 0
		else:
			direction = position - lastpos
	
	var animation
	if abs(direction.x) > abs(direction.y):
		if direction.x < 0:
			animation = "walk_left"
		else:
			animation = "walk_right"
	else:
		if direction.y < 0:
			animation = "walk_up"
		else:
			animation = "walk_down"
	
	var animplayer = $Sprite/AnimationPlayer
	if animplayer.current_animation != animation:
		animplayer.play(animation)
	if current_speed == 0:
		animplayer.stop()

func start_walk():
	var path = get_parent()
	if !path || path.get_class() != "Path2D" || !path.curve || path.curve.get_point_count() < 2:
		return
	offset = 0
	current_speed = speed
